using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using Holoville.HOTween;
using Holoville.HOTween.Plugins;

/// <summary>
/// Tile cell effect and touch/click event.
/// </summary>
public class MatchItem : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {
	public GameObject target;
	public Cell cell;
	public TilePoint point;
    public bool isUp = false;
    public Vector2 delta;
    Image icon, choice;

    public void OnPointerDown(PointerEventData eventData)
    {
        if (target == null) return;
        isUp = false;
        if (target) target.SendMessage("OnClickAction", this, SendMessageOptions.DontRequireReceiver);
        Debug.Log("position: " + eventData.position.ToString() + " / pressPosition: " + eventData.pressPosition.ToString());
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (target == null) return;
        isUp = true;
        delta = eventData.position - eventData.pressPosition;
        if (target) target.SendMessage("OnClickAction", this, SendMessageOptions.DontRequireReceiver);
        Debug.Log("position: " + eventData.position.ToString() + " / pressPosition: " + eventData.pressPosition.ToString());
    }

    public void OnMouseDown () {
		if (target) target.SendMessage("OnClickAction", this, SendMessageOptions.DontRequireReceiver);
	}

    public void SetChoice(bool isOn)
    {
        choice.enabled = isOn;
    }

    public void HideChoice(float dtime)
    {
        StartCoroutine(DelayAction(dtime, () =>
        {
            SetChoice(false);
        }));

    }

    public void DrawHint()
    {
        TweenParms parms = new TweenParms().Prop("color", new Color(1f,1f,1f,0.8f)).Ease(EaseType.EaseOutQuad).Loops(6, LoopType.Yoyo);
        HOTween.To(icon, 0.2f, parms);
        parms = new TweenParms().Prop("localScale", Vector3.one * 1.2f).Ease(EaseType.EaseOutQuad).Loops(6, LoopType.Yoyo);
        HOTween.To(transform, 0.2f, parms);
    }

	void Start () {
        choice = transform.Find("Choice").GetComponent<Image>();
        icon = GetComponent<Image>();
        SetChoice(false);
	}

    IEnumerator DelayAction(float dTime, System.Action callback)
    {
        yield return new WaitForSeconds(dTime);
        callback();
    }
}
