using UnityEngine;
using System.Collections;

/// <summary>
/// To destroy game object with delay time.
/// </summary>
public class DelayDestroy : MonoBehaviour {
	public float delayTime = 2f;

	void OnEnable ()
	{
		StartCoroutine (Check ());
	}

	IEnumerator Check () {
		yield return new WaitForSeconds (delayTime);
		if (gameObject.name != "Respawn")
			Destroy (gameObject);
		else {
			gameObject.SetActive (false);
		}
	}
}
