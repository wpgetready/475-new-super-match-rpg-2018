﻿using UnityEngine;
using System.Collections;

public class SpriteScroller : MonoBehaviour {
	public Vector3 speed = new Vector3(-10f, 0f, 0f);
    Vector3 oldPosition;
    Transform tf;

	void Start () {
        tf = transform;
        oldPosition = tf.localPosition;
	}
	
	void Update () {
        if (transform.localPosition.x < -3200f) transform.localPosition = oldPosition;
        tf.localPosition += speed * Time.deltaTime;
	}
}
