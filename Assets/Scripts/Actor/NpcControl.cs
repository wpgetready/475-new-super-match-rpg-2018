using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Holoville.HOTween;
using Holoville.HOTween.Plugins;

/// <summary>
/// To draw damage motion with npc monster.
/// </summary>
public class NpcControl : MonoBehaviour {
    public GameObject slashEffect;
    public GameObject bloodEffect;
    public Slider hpBar;
    public SpriteRenderer idleSprite, attackSprite, damageSprite;
	[HideInInspector]
	public SpriteRenderer sRender;
	[HideInInspector]
	public Sprite headStone;
	float healthPoint = 1f;
	public GameManager gameManager;
	public Transform respawnPos;
	[HideInInspector]
	public Animator animator;

	private int typeAnim;
    
    void Start()
    {
		typeAnim = Menu.selectedLevel+1;
        animator = GetComponent<Animator>();
        sRender = GetComponent<SpriteRenderer>();
//		animator.Play("Idle"+(Menu.selectedLevel+1.ToString());

		switch (Menu.selectedLevel+1) {
		case 5:
			animator.Play("Idle9");
			break;
		case 10:
			animator.Play("Idle10");
			break;
		case 15:
			animator.Play("Idle11");
			break;
		case 20:
			animator.Play("Idle12");
			break;
		case 25:
			animator.Play("Idle13");
			break;
		default:
			animator.Play("Idle"+(Menu.selectedLevel+1).ToString());
			break;
		}
    }

    IEnumerator DoneAttack(float delayTime)
    {
        yield return new WaitForSeconds(delayTime);
        if (idleSprite) idleSprite.enabled = true;
        if (attackSprite) attackSprite.enabled = false;
    }

    IEnumerator DoneDamage(float delayTime)
    {
		yield return new WaitForSeconds(delayTime);
        if (idleSprite) idleSprite.enabled = true;
        if (damageSprite) damageSprite.enabled = false;
	}

    IEnumerator DoAttack(float delayTime)
    {
        if (idleSprite) idleSprite.enabled = false;
        if (attackSprite) attackSprite.enabled = true;
        yield return new WaitForSeconds(delayTime);
        GameObject instance = Instantiate(slashEffect) as GameObject;
        instance.transform.parent = transform.parent;
        instance.transform.localPosition = new Vector3(10f, 0f, 0f);
    }

	IEnumerator DoDamage(float delayTime) {
        if (damageSprite) damageSprite.enabled = true;
        if (idleSprite) idleSprite.enabled = false;
		yield return new WaitForSeconds(delayTime);
        GameObject instance = Instantiate(bloodEffect) as GameObject;
        instance.transform.parent = transform.parent;
        if (sRender)
            instance.transform.localPosition = transform.localPosition + new Vector3(0f, 20f, 0f);
        else
            instance.transform.localPosition = transform.localPosition + new Vector3(0f, 100f, 0f);
    }
	
	void SetHealthPoint(float point){
		if (point < 0f) {
			gameManager.pcControl.animator.Play ("pc_run");
			gameManager.nextLevel = true;
			animator.enabled = false;
			sRender.sprite = headStone;
			if (Menu.selectedLevel == PlayerPrefs.GetInt ("level")) {
				PlayerPrefs.SetInt ("level", PlayerPrefs.GetInt ("level") + 1);
			}
		}
		if (point>1f) point = 1f;
		TweenParms parms = new TweenParms().Prop("value", point).Ease(EaseType.EaseOutQuart);
		HOTween.To(hpBar, 0.1f, parms );
		healthPoint = point;
	}

	void Update ()
	{
		if (gameManager.nextLevel) {
			foreach (var tile in gameManager.myLineList) {
				if (tile != null) {
					Destroy (tile.gameObject);
				}
			}

			if (gameManager.fillAttackSlider.activeInHierarchy) {
				gameManager.fillAttackSlider.SetActive (false);
			}

			transform.localPosition = new Vector3 (transform.localPosition.x - 1, transform.localPosition.y, transform.localPosition.z);
			gameManager.floor.transform.localPosition = new Vector3 (gameManager.floor.transform.localPosition.x - 1, gameManager.floor.transform.localPosition.y, gameManager.floor.transform.localPosition.z);
			if (transform.localPosition.x < -400f) {
				
				gameManager.pcControl.animator.Play ("Idle");

				sRender.enabled = false;

				gameManager.nextLevel = false;
			
				gameManager.myLineList.Clear ();


				if (GameManager.currentLevel % 5 == 0) {
					gameManager.totalBossKill += 1;
				} else {
					gameManager.totalKill += 1;
				}

				GameObject effect;
				effect = Instantiate (gameManager.respawnEffect, transform.position, transform.rotation);
				effect.transform.SetParent (transform.parent);
				effect.transform.localPosition = new Vector3 (230f, -50f, 0);

				transform.localPosition = new Vector3 (230f, -50f, 0);

				Invoke("Respawn",1);

				SetHealthDamage (-10);
			}
		}
	}
	void Respawn()
	{
		gameManager.InitArena ();

		gameManager.fillAttackSlider.SetActive (true);
		gameManager.fillImage.fillAmount = 1;

		typeAnim = Random.Range (1, 9);
		animator.enabled = true;
//		GameManager.currentLevel += 1;
		Menu.selectedLevel+=1;
		switch (Menu.selectedLevel+1) {
		case 5:
			animator.Play("Idle9");
			break;
		case 10:
			animator.Play("Idle10");
			break;
		case 15:
			animator.Play("Idle11");
			break;
		case 20:
			animator.Play("Idle12");
			break;
		case 25:
			animator.Play("Idle13");
			break;
		default:
			animator.Play("Idle"+(Menu.selectedLevel+1).ToString());
			break;
		}
		gameManager.SetLevelText ();
	}
	void SetHealthDamage(float damage){
		SetHealthPoint(healthPoint - damage);
	}

	public void Damage(){
//        if (animator) animator.CrossFade("Damage", 0.2f);
//		animator.Play("Damage"+GameManager.currentLevel.ToString());
		switch (Menu.selectedLevel+1) {
		case 5:
			animator.Play("Damage9");
			break;
		case 10:
			animator.Play("Damage10");
			break;
		case 15:
			animator.Play("Damage11");
			break;
		case 20:
			animator.Play("Damage12");
			break;
		case 25:
			animator.Play("Damage13");
			break;
		default:
			animator.Play("Damage"+(Menu.selectedLevel+1).ToString());
			break;
		}
        StartCoroutine(DoDamage(0.1f));
		StartCoroutine( DoneDamage(0.1f) );
		SetHealthDamage(gameManager.level[GameManager.currentLevel-1].damageToEnemy);
	}
    public void Attack()
    {
//        if (animator) animator.CrossFade("Attack", 0.2f);
		animator.Play("Attack"+Menu.selectedLevel+1.ToString());
		switch (Menu.selectedLevel+1) {
		case 5:
			animator.Play("Attack9");
			break;
		case 10:
			animator.Play("Attack10");
			break;
		case 15:
			animator.Play("Attack11");
			break;
		case 20:
			animator.Play("Attack12");
			break;
		case 25:
			animator.Play("Attack13");
			break;
		default:
			animator.Play("Attack"+(Menu.selectedLevel+1).ToString());
			break;
		}
        StartCoroutine(DoAttack(0.5f));
        StartCoroutine(DoneAttack(0.5f));
    }
}
