using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Holoville.HOTween;
using Holoville.HOTween.Plugins;

/// <summary>
/// To draw attack animation with player.
/// </summary>
public class PcControl : MonoBehaviour {
	public GameObject slashEffect;
	public GameObject thunderEffect;
    public GameObject bloodEffect;
	public GameObject thunder1Effect;
	public GameObject explosionItem;
	public GameObject superThunderItem;
	public GameObject boomItem;
    public Slider hpBar;
    public SpriteRenderer idleSprite, attackSprite, damageSprite;
	SpriteRenderer sRender;
	public Sprite deadSpr;
	public GameManager gameManager;
	float healthPoint = 1f;

	public Animator animator;


public enum AttackType
	{
		sword,
		thunder,
		sheild,
		gem,
		blood
	}

	public AttackType _AttackType;
    
    void Start()
    {
        animator = GetComponent<Animator>();
        sRender = GetComponent<SpriteRenderer>();
    }

	IEnumerator DoneAttack(float delayTime) {
		yield return new WaitForSeconds(delayTime);
        if (idleSprite) idleSprite.enabled = true;
		if (attackSprite) attackSprite.enabled = false;
	}

    IEnumerator DoneDamage(float delayTime)
    {
        yield return new WaitForSeconds(delayTime);
        if (idleSprite) idleSprite.enabled = true;
        //if (damageSprite) damageSprite.enabled = false;
    }

	IEnumerator DoAttack(float delayTime) {
        if (idleSprite) idleSprite.enabled = false;
        if (attackSprite) attackSprite.enabled = true;
		yield return new WaitForSeconds(delayTime);
		if (!gameManager.attackType) {
			switch (_AttackType) {
			case AttackType.sword:
				GameObject instance = Instantiate (slashEffect) as GameObject;
				instance.transform.parent = transform.parent;
				instance.transform.localPosition = new Vector3 (20f, 0f, 0f);
				break;
			case AttackType.thunder:
				GameObject instance1 = Instantiate (thunderEffect) as GameObject;
				instance1.transform.parent = transform.parent;
				instance1.transform.localPosition = new Vector3 (-7500f, 2800f, 600f);
				instance1.transform.localScale = Vector3.one * 700;
				break;
			case AttackType.blood:
				SetHealthDamage (-0.1f);
				break;
			}
		} else if (gameManager.attackType) {
			gameManager.attackType = false;

			switch (gameManager.itemType) {
			case 1:
				GameObject instance1 = Instantiate (superThunderItem) as GameObject;
				instance1.transform.parent = transform.parent;
				instance1.transform.localPosition = new Vector3 (-7500f, 1800, 600f);
				instance1.transform.localScale = Vector3.one * 700;
				break;
			case 2:
				GameObject instance2 = Instantiate (explosionItem) as GameObject;
				instance2.transform.parent = transform.parent;
				instance2.transform.localPosition = new Vector3 (-7500f, 2800, 600f);
				instance2.transform.localScale = Vector3.one * 700;
				break;
			case 3:
				GameObject instance3 = Instantiate (thunder1Effect) as GameObject;
				instance3.transform.parent = transform.parent;
				instance3.transform.localPosition = new Vector3 (-7500f, 2800, 600f);
				instance3.transform.localScale = Vector3.one * 700;
				break;
			case 4:
				gameManager.giveDamage = false;
				SetHealthDamage (-0.3f);
				GameObject instance4 = Instantiate (boomItem) as GameObject;
				instance4.transform.parent = transform.parent;
				instance4.transform.localPosition = new Vector3 (-9000f, 1100, 600f);
				instance4.transform.localScale = Vector3.one * 700;
				break;
			}
		}
    }



    IEnumerator DoDamage(float delayTime)
    {
        //if (damageSprite) damageSprite.enabled = true;
        if (idleSprite) idleSprite.enabled = false;
        yield return new WaitForSeconds(delayTime);
        GameObject instance = Instantiate(bloodEffect) as GameObject;
        instance.transform.parent = transform.parent;
        instance.transform.localRotation = Quaternion.Euler(-45f, -140f, 0f); ;
        if (sRender)
            instance.transform.localPosition = transform.localPosition + new Vector3(0f, 20f, 0f);
        else
            instance.transform.localPosition = transform.localPosition + new Vector3(0f, 100f, 0f);
    }

	void SetHealthPoint(float point){
		if (point <= 0f) {
			gameManager.SetGameResult ();
			animator.enabled = false;
			sRender.sprite = deadSpr;
			gameManager.isGamePause = true;
			Invoke ("GameOver", 1f);
		}
		if (point>1f) point = 1f;
		TweenParms parms = new TweenParms().Prop("value", point).Ease(EaseType.EaseOutQuart);
		HOTween.To(hpBar, 0.1f, parms );
		healthPoint = point;
	}

	void GameOver()
	{
		AdsControl.Instance.showAds ();
		gameManager.gameOverPanel.SetActive (true);
		gameManager.npcControl.gameObject.SetActive (false);
		gameObject.SetActive (false);
	}

	void SetHealthDamage(float damage){
		SetHealthPoint(healthPoint - damage);
	}

	public void Attack(){
        if (animator) animator.CrossFade("Attack", 0.2f);
        StartCoroutine(DoAttack(0.5f));
		StartCoroutine( DoneAttack(0.5f) );
	}
    public void Damage()
    {
		if (!gameManager.isProtected) {
			if (animator)
				animator.CrossFade ("Damage", 0.2f);
			StartCoroutine (DoDamage (0.1f));
			StartCoroutine (DoneDamage (0.1f));
			SetHealthDamage (gameManager.level [GameManager.currentLevel - 1].damageToPlayer);
		}
    }
}
