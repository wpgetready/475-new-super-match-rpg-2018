﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using Holoville.HOTween;
using Holoville.HOTween.Plugins;
using UnityEngine.UI;

[System.Serializable]
public class Level
{
	public float enemyAttackFillSpeed;
	public float damageToEnemy;
	public float damageToPlayer;
}

/// <summary>
/// manage tile grid
/// </summary>
public class GameManager : MonoBehaviour {
	public static int currentLevel = 1;
    // tile prefab
    public GameObject cellItemPrefab;
    public Transform grid, effectArea;
    public PcControl pcControl;
    public NpcControl npcControl;
    public GameObject explosionPrefab;
    public GameObject[] starEffectPrefabs;

    // tile lines
    Transform[] lines;

    // tile items
    public Tile[] items;

    // auto clear match tiles game
    public bool isAuto = false;

    // tile size
    public Vector3 cellSize = new Vector3(1f, 0.96f, 1f);

    // tile scale
    public Vector3 cellScale = new Vector3(1f, 1f, 1f);

	List<Tile> choiceList;

    Line[] lineList;

    public Polyline polyLine;

    int totalCols = 7;
    int totalRows = 6;

    string[] sounds = new string[] { "cash", "perc", "coo", "glass", "water", "fx" };
    AudioClip[] audioMatchClip = null;
    AudioSource[] audioMatchSource = null;

	[Header("Enemy Fill Attack")]
	public Image fillImage;
	public float fillSpeed = 0.05f;
	private bool attack;

	[Header("Effect Pos")]
	public Transform pos;
	public Transform pos1;
	public Transform pos2;
	public Transform pos3;
	public GameObject effect;
	public Text valueText;
	public Text value1Text;
	public Text value2Text;
	public Text value3Text;
	private int count;

	[Header("Pause Game")]
	public GameObject pausePanel;
	public bool isGamePause;
	[Header("Game Over")]
	public GameObject gameOverPanel;
	public Text levelText;
	public Text killsText;
	public Text bossText;

	[Header("CheckWave")]
	public List<Line> myLineList;

	[Header("Next Level Param")]
	public bool nextLevel;
	public GameObject floor;
	public GameObject respawnEffect;

	[Header("Level Param")]
	public Level[] level;
	public Text levelIndex;
	[HideInInspector]
	public int totalKill;
	[HideInInspector]
	public int totalBossKill;

	public Image audioBtnImg;
	public Sprite audioOn, audioOff;
	public GameObject fillAttackSlider;

	public Text coinText;

	public GameObject sheild;
	public int timeProtect;
	public bool isProtected;
    void Start()
    {
        choiceList = new List<Tile>();
        SetupAudioSource();
        InitArena();
        isInput = true;
		SetItemText ();
		SetLevelText ();
		AudioSystem ();
		SetCoinText ();
		FireBase._instance.LogLevel ((Menu.selectedLevel + 1));
//		print (myLineList [0].gameObject.transform.GetChild(0).gameObject.transform.GetChild(2).GetComponent<Image>().sprite.name);
    }

    // Setup Audio Source.
    void SetupAudioSource()
    {
        audioMatchClip = new AudioClip[sounds.Length];
        audioMatchSource = new AudioSource[sounds.Length];
        for (int i = 0; i < sounds.Length; i++)
        {
            audioMatchClip[i] = Resources.Load("Sounds/" + sounds[i], typeof(AudioClip)) as AudioClip;
            audioMatchSource[i] = gameObject.AddComponent<AudioSource>();
            audioMatchSource[i].clip = audioMatchClip[i];
        }
    }

	void AudioSystem()
	{
		if (PlayerPrefs.GetInt ("audio") == 0) {
			audioBtnImg.sprite = audioOn;
		} else if (PlayerPrefs.GetInt ("audio") == 1) {
			audioBtnImg.sprite = audioOff;
		}
	}

	public void SetUpAudio()
	{
		if (PlayerPrefs.GetInt ("audio") == 0) {
			print (0);
			PlayerPrefs.SetInt ("audio", 1);
			AudioSystem ();
			return;
		}
		if (PlayerPrefs.GetInt ("audio") == 1) {
			print (1);
			PlayerPrefs.SetInt ("audio", 0);
			AudioSystem ();
		}
	}

    public void OnPointerExit(PointerEventData eventData)
    {
        DoneDrag();
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        PointerEventData pointer = eventData;
        List<RaycastResult> raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointer, raycastResults);
        foreach (RaycastResult rr in raycastResults)
        {
            Tile tile = rr.gameObject.GetComponent<Tile>();
            if (tile)
            {
                isDrag = true;
                ChoiceCell(tile, tile.lineScript.idx, tile.idx, tile.GetTileType(), tile.isMove);
                break;
            }
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        PointerEventData pointer = eventData;
        List<RaycastResult> raycastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointer, raycastResults);
        foreach (RaycastResult rr in raycastResults)
        {
            Tile tile = rr.gameObject.GetComponent<Tile>();
            if (tile)
            {
                ChoiceCell(tile, tile.lineScript.idx, tile.idx, tile.GetTileType(), tile.isMove);
                break;
            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        DoneDrag();
    }

    Vector3 CalcX(int x)
    {
        return (x - totalCols / 2) * Vector3.right * cellSize.x + Vector3.down * (totalRows + x % 2 - 1.5f) * 0.5f * cellSize.y;
    }

    Vector3 CalcY(int y)
    {
        return Vector3.up * y * cellSize.y;
    }

    Vector3 CalcXY(int x, int y)
    {
        return CalcX(x) + CalcY(y);
    }


    // init game arena, draw tile grid
	public void InitArena()
    {
        lines = new Transform[totalCols];
        lineList = new Line[lines.Length];

        // tile line loop
        for (int i = 0; i < lines.Length; i++)
        {
            GameObject go = new GameObject();
            go.transform.SetParent(grid);
            Line script = go.AddComponent<Line>();
            script.idx = i;
            Transform tf = go.transform;
            lines[i] = tf;
            tf.SetParent(grid);
            tf.localScale = Vector3.one;
            tf.localPosition = CalcX(i);
            go.name = "Line" + (i + 1).ToString("000");
            go.layer = gameObject.layer;

            script.items = new Tile[totalRows];

			myLineList.Add (go.GetComponent<Line>());
            // tile loop in some line 
            for (int j = 0; j < script.items.Length; j++)
            {
                GameObject g = Instantiate(cellItemPrefab) as GameObject;
                g.name = "Tile" + (j + 1).ToString("000");
                Transform t = g.transform;
                Tile c = g.GetComponent<Tile>();
                c.height = cellSize.y;
                c.gameManager = this;
                c.SetTileType(Random.Range(0, 5) % 5);
                c.lineScript = script;
                script.items[j] = c;
                c.idx = j;
                t.SetParent(tf);
                t.localPosition = CalcY(j);
                t.localScale = cellScale;
                t.localRotation = Quaternion.identity;
            }
            script.idx = i;
            lineList[i] = script;
        }

        items = GetComponentsInChildren<Tile>();
    }

    // find near neighbor six tiles
    void InitNears()
    {
        for (int i = 0; i < lineList.Length; i++)
        {
            Line ln = lineList[i];
            for (int j = 0; j < ln.items.Length; j++)
            {
                Tile c = ln.items[j];
                c.nearTiles = new List<Tile>();
                if (i + 1 < lines.Length)
                {
                    c.nearTiles.Add(lineList[(i + 1) % lineList.Length].items[j]);
                    if (i % 2 == 1 && j - 1 >= 0)
                        c.nearTiles.Add(lineList[(i + 1) % lineList.Length].items[j - 1]);
                    if (i % 2 == 0 && j + 1 < ln.items.Length)
                        c.nearTiles.Add(lineList[(i + 1) % lineList.Length].items[j + 1]);
                }
                if (i - 1 >= 0)
                {
                    c.nearTiles.Add(lineList[(i + lineList.Length - 1) % lineList.Length].items[j]);
                    if (i % 2 == 1 && j - 1 >= 0)
                        c.nearTiles.Add(lineList[(i + lineList.Length - 1) % lineList.Length].items[j - 1]);
                    if (i % 2 == 0 && j + 1 < ln.items.Length)
                        c.nearTiles.Add(lineList[(i + lineList.Length - 1) % lineList.Length].items[j + 1]);
                }
                if (j - 1 >= 0)
                    c.nearTiles.Add(lineList[i].items[j - 1]);
                if (j + 1 < ln.items.Length)
                    c.nearTiles.Add(lineList[i].items[j + 1]);
            }
        }
    }

    void DrawPolyLine()
    {
        if (choiceList.Count < 2)
        {
            polyLine.SetVisible(false);
            return;
        }
        Vector3[] path = new Vector3[choiceList.Count];
        for (int i = 0; i < choiceList.Count; i++)
        {
            Tile cb = choiceList[i];
            Vector3 pos = CalcXY(cb.lineScript.idx, cb.idx);
            pos += grid.localPosition;
            path[i] = pos;
        }
        polyLine.path = path;
        polyLine.InitLines();
        polyLine.DrawLines();
    }

    // choice tile
    public void ChoiceCell(Tile cell, int x, int y, int type, bool isMove)
    {
        if (oldX == x && oldY == y) return;
        if ((oldType != -1 && oldType != type) || choiceList.Contains(cell))
        {
            DoneDrag();
            cell.UnSetChoice();
            return;
        }
        if (isMove) return;
        choiceList.Add(cell);
//        DrawPolyLine();
        cell.isMove = true;
        cell.SetChoice();
        oldX = x;
        oldY = y;
        oldType = type;
    }

    bool isDrag = false, isInput = false;
    int oldX = -1, oldY = -1, oldType = -1;

    // clear match tiles & sort tile grid when dragged
    void DoneDrag()
    {
        isDrag = false;
        oldX = -1;
        oldY = -1;
        oldType = -1;
        if (choiceList.Count < 3)
        {
            foreach (Tile cb in choiceList)
            {
                cb.isMove = false;
                cb.UnSetChoice();
            }
            choiceList.Clear();
            DrawPolyLine();
            return;
        }
       

        foreach (Tile cb in choiceList)
        {
            GameObject instance = Instantiate(explosionPrefab) as GameObject;
            instance.transform.SetParent(effectArea);
            Vector3 pos = CalcXY(cb.lineScript.idx, cb.idx);
            pos += grid.localPosition;
            instance.transform.localPosition = pos;
			if (PlayerPrefs.GetInt ("audio") == 0) {
				audioMatchSource [cb.GetTileType ()].Play ();
			}
			switch (cb.shapeRenderer.sprite.name) {
			case "sword":
				pcControl._AttackType = PcControl.AttackType.sword;
				StartCoroutine(AttackMonster(0.7f));
				break;
			case "sheild":
				pcControl._AttackType = PcControl.AttackType.sheild;
				timeProtect += 1;
//				AddItemValue (1);
				break;
			case "gem":
				pcControl._AttackType = PcControl.AttackType.gem;
				PlayerPrefs.SetInt ("coin", PlayerPrefs.GetInt ("coin") + 1);
				SetCoinText ();
//				AddItemValue (2);
				break;
			case "blood":
				pcControl._AttackType = PcControl.AttackType.blood;
				StartCoroutine(AttackMonster(0.7f));
				break;
			case "thunder":
				pcControl._AttackType = PcControl.AttackType.thunder;
//				AddItemValue (0);
				StartCoroutine(AttackMonster(0.7f));
				break;
			}
			DoStarEffect(pos, cb.GetTileType());
//			Time.timeScale = 0;
        }

        choiceList.Clear();
//        DrawPolyLine();
        foreach (Transform item in lines)
            item.SendMessage("SortCells", SendMessageOptions.DontRequireReceiver);

        isInput = false;
        StartCoroutine(DelayAction(1f, () =>
        {
            isInput = true;
        }));

		if (timeProtect != 0) {
			isProtected = true;
			sheild.SetActive (true);
			Invoke ("StopProtect", timeProtect);
			timeProtect = 0;
		}

    }

	// stop protect

	void StopProtect()
	{
		isProtected = false;
		sheild.SetActive (false);
	}
    // delay method coroutine
    IEnumerator DelayAction(float dTime, System.Action callback)
    {
        yield return new WaitForSeconds(dTime);
        callback();
    }

    // auto clear match tiles and go next turn
//    void DoAutoClear(Tile Tile)
//    {
//        Stack<Tile> stack = new Stack<Tile>();
//        List<Tile> searchList = new List<Tile>();
//
//        InitNears();
//
//        choiceList.Add(Tile);
//        searchList.Add(Tile);
//
//        // find first near match tile
//        foreach (Tile cb in Tile.nearTiles)
//        {
//            searchList.Add(cb);
//            if (cb.GetTileType() == Tile.GetTileType())
//            {
//                stack.Push(cb);
//                choiceList.Add(cb);
//            }
//        }
//
//        // find all near match tile
//        while (stack.Count > 0)
//        {
//            foreach (Tile cb in stack.Pop().nearTiles)
//            {
//                if (searchList.Contains(cb)) continue;
//                searchList.Add(cb);
//                if (cb.GetTileType() != Tile.GetTileType()) continue;
//                stack.Push(cb);
//                if (!choiceList.Contains(cb)) choiceList.Add(cb);
//            }
//        }
//
//        // match tile clear and sort tiles
//        foreach (Tile cb in choiceList)
//            cb.isMove = true;
//        foreach (Transform item in lines)
//            item.SendMessage("SortCells", SendMessageOptions.DontRequireReceiver);
//        choiceList.Clear();
//
//        // when ready to input
//        isInput = false;
//        StartCoroutine(DelayAction(1f, () =>
//        {
//            isInput = true;
//        }));
//    }

    IEnumerator RandomMonsterAttack(float delayTime)
    {
        yield return new WaitForSeconds(delayTime);
        StartCoroutine(AttackPlayer(0.7f));
        StartCoroutine(RandomMonsterAttack(Random.Range(3f, 6f)));
    }

    // Attack NPC Monster
    IEnumerator AttackMonster(float delayTime)
    {
        pcControl.Attack();
        yield return new WaitForSeconds(delayTime);
		if (pcControl._AttackType == PcControl.AttackType.sword || pcControl._AttackType == PcControl.AttackType.thunder) {
			if (giveDamage) {
				npcControl.Damage ();
			} else if (!giveDamage) {
				giveDamage = true;
			}
		}
    }

    IEnumerator AttackPlayer(float delayTime)
    {
        npcControl.Attack();
        yield return new WaitForSeconds(delayTime);
        pcControl.Damage();
    }
		

    // Star Flash Effect

    void DoStarEffect(Vector3 pos, int type)
    {
        if (type < 0) return;
        GameObject starEffectObject = Instantiate(starEffectPrefabs[type]) as GameObject;
        Transform starEffect = starEffectObject.transform;
        starEffect.SetParent(effectArea.transform);
		if (pcControl._AttackType == PcControl.AttackType.blood || pcControl._AttackType == PcControl.AttackType.sheild) {
			Vector3[] path = new Vector3[] { pos, new Vector3 (0, 100, 0), new Vector3 (-150, 350, 0) };
			starEffect.localPosition = pos;
			HOTween.To (starEffect, 1f, new TweenParms ().Prop ("localPosition", new PlugVector3Path (path, EaseType.Linear, true)));
		} else if (pcControl._AttackType == PcControl.AttackType.sword || pcControl._AttackType == PcControl.AttackType.thunder) {
			Vector3[] path = new Vector3[] { pos, new Vector3 (0, 100, 0), new Vector3 (200, 350, 0) };
			starEffect.localPosition = pos;
			HOTween.To (starEffect, 1f, new TweenParms ().Prop ("localPosition", new PlugVector3Path (path, EaseType.Linear, true)));
		} else if (pcControl._AttackType == PcControl.AttackType.gem) {
			Vector3[] path = new Vector3[] { pos, new Vector3 (0, 100, 0), new Vector3 (-300, 650, 0) };
			starEffect.localPosition = pos;
			HOTween.To (starEffect, 1f, new TweenParms ().Prop ("localPosition", new PlugVector3Path (path, EaseType.Linear, true)));
		}
	}

	void AttackPlayer()
	{
		npcControl.Attack();
		Invoke ("GivePlayerDamage", 0.5f);
	}
	void GivePlayerDamage()
	{
		pcControl.Damage();
	}
    void Update()
    {
		if (!isGamePause) {
			fillImage.fillAmount -= level[currentLevel-1].enemyAttackFillSpeed;
		}
		if (fillImage.fillAmount <= 0) {
			attack = true;
		}

		if (attack) {
			if (!nextLevel) {
				AttackPlayer ();
			}
			attack = false;
			fillImage.fillAmount = 1;
		}
    }

	void DoItemEffect(int index)
	{
		switch (index) {
		case 0://super thunder
			Instantiate(effect,pos.position,Quaternion.identity);
			break;
		case 1: //explosion
			Instantiate (effect, pos1.position, Quaternion.identity);
			break;
		case 2: //thunder
			Instantiate (effect, pos2.position, Quaternion.identity);
			break;
		case 3: //boom			
			Instantiate (effect, pos3.position, Quaternion.identity);
			break;
		}
		SetItemText ();
//		Time.timeScale = 0;
	}
	void SetItemText()
	{
		valueText.text = PlayerPrefs.GetInt ("superthunder").ToString ();
		value1Text.text = PlayerPrefs.GetInt ("explosion").ToString ();
		value2Text.text = PlayerPrefs.GetInt ("thunder").ToString ();
		value3Text.text = PlayerPrefs.GetInt ("boom").ToString ();
	}
	public void SetLevelText()
	{
		levelIndex.text = "Level: " + (Menu.selectedLevel+1).ToString ();
	}
	public	void SetGameResult()
	{
		levelText.text = "Level: "+(Menu.selectedLevel+1).ToString ();
		killsText.text = "Kills: " + totalKill.ToString ();
		bossText.text = "Boss: " + totalBossKill.ToString ();
	}
	public void PlayAgain()
	{
		gameOverPanel.SetActive (false);
		Time.timeScale = 1;
		Application.LoadLevel (Application.loadedLevel);
	}
	void AddItemValue(int index)
	{
		count += 1;
		if (count == choiceList.Count && count > 2) {
			
			count = 0;
			switch (index) {
			case 0://super thunder
				switch (Random.Range (0, 2)) {
				case 0:
//					PlayerPrefs.SetInt ("superthunder", PlayerPrefs.GetInt ("superthunder") + 1);
					DoItemEffect (0);
					break;
				case 1:
//					PlayerPrefs.SetInt ("boom", PlayerPrefs.GetInt ("boom") + 1);
					DoItemEffect (3);
					break;
				}
				break;
			case 1: //explosion
//				PlayerPrefs.SetInt ("explosion", PlayerPrefs.GetInt ("explosion") + 1);
				DoItemEffect (1);
				break;
			case 2: //thunder
//				PlayerPrefs.SetInt ("thunder", PlayerPrefs.GetInt ("thunder") + 1);
				DoItemEffect (2);
				break;
			}
		}
	}
	void SetCoinText()
	{
		coinText.text = PlayerPrefs.GetInt ("coin").ToString ();
	}
	[Header("Item")]
	public int itemType;
	public bool attackType;
	public bool giveDamage = true;
	//false - attack normal
	//true - use item attack
	public void UseItem(int index)
	{
		itemType = index;
		attackType = true;

		switch (index) {
		case 1:
			if (PlayerPrefs.GetInt ("superthunder") > 0) {
				PlayerPrefs.SetInt ("superthunder", PlayerPrefs.GetInt ("superthunder") - 1);
				StartCoroutine(AttackMonster(0.7f));
			}
			break;
		case 2:
			if (PlayerPrefs.GetInt ("explosion") > 0) {
				PlayerPrefs.SetInt ("explosion", PlayerPrefs.GetInt ("explosion") - 1);
				StartCoroutine(AttackMonster(0.7f));
			}
			break;
		case 3:
			if (PlayerPrefs.GetInt ("thunder") > 0) {
				PlayerPrefs.SetInt ("thunder", PlayerPrefs.GetInt ("thunder") - 1);
				StartCoroutine(AttackMonster(0.7f));
			}
			break;
		case 4:
			if (PlayerPrefs.GetInt ("boom") > 0) {
				PlayerPrefs.SetInt ("boom", PlayerPrefs.GetInt ("boom") - 1);
				StartCoroutine(AttackMonster(0.7f));
			}
			break;
		}
		SetItemText ();
	}
	public void PauseGame(int index)
	{
		switch (index) {
		case 0:
			pcControl.gameObject.SetActive (false);
			npcControl.gameObject.SetActive (false);
			Time.timeScale = 0;
			isGamePause = true;
			pausePanel.SetActive (true);
			AdsControl.Instance.showAds ();
			break;
		case 1:
			Time.timeScale = 1;
			Application.LoadLevel ("Menu");
			break;
		case 2:
			pcControl.gameObject.SetActive (true);
			npcControl.gameObject.SetActive (true);
			Time.timeScale = 1;
			isGamePause = false;
			pausePanel.SetActive (false);
			break;
		case 3:
			Time.timeScale = 0;
			Application.LoadLevel (Application.loadedLevel);
			break;
		}
	}
}
