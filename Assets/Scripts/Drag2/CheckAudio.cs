﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckAudio : MonoBehaviour {

	IEnumerator Start ()
	{
		yield return new WaitForEndOfFrame ();
		if (PlayerPrefs.GetInt ("audio") == 0) {
			GetComponent<AudioSource> ().Play ();
		}	
	}
}
