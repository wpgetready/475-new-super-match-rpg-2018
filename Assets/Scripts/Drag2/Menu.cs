﻿using UnityEngine.UI;
using UnityEngine;

public class Menu : MonoBehaviour {
	public static Menu instance;
	public static int selectedLevel;
	public Image soundImage;
	public Sprite soundOn, soundOff;
	public Animator anim;
	public AudioSource source;
	public AudioClip menuBg;


	///<Summary>
	///Level Selector
	///</Summary>
	public GameObject levelSelector,mainMenu,shopPanel;
	public Text[] levelBtnIndex;
	public GameObject[] levelBtn;
	public Button[] levelBtnBtn;


	///<Summary>
	///Shop
	///</Summary>

	public Text coinText;
	public Text itemValueTxt,itemValueTxt1,itemValueTxt2,itemValueTxt3;
	public Text[] shopItemValueText;
	public int[] shopItemValue;
	public string[] itemName;

	private	void Awake ()
	{
		instance = this;
		anim.Play ("Menu");
		SetUpAudio ();	
//		PlayerPrefs.DeleteAll ();
//		PlayerPrefs.SetInt ("coin", 10000);

		SetText ();
		SetItemValue ();
	}

	void Start ()
	{
		PlayBgSound ();
		SetLevelNumber ();
		FireBase._instance.LogHome ();
	}
	void SetUpAudio()
	{
		if (PlayerPrefs.GetInt ("audio") == 0) {
			soundImage.sprite = soundOn;
		} else if (PlayerPrefs.GetInt ("audio") == 1) {
			soundImage.sprite = soundOff;
		}
	}

	public void SetAudio()
	{
		if (PlayerPrefs.GetInt ("audio") == 0) {
			PlayerPrefs.SetInt ("audio", 1);
		} else if (PlayerPrefs.GetInt ("audio") == 1) {
			PlayerPrefs.SetInt ("audio", 0);
		}
		SetUpAudio ();
	}

	public void StartGame(int index)
	{
		switch (index) {
		case 0:
			mainMenu.SetActive (false);
			levelSelector.SetActive (true);
			break;
		case 1:
			mainMenu.SetActive (true);
			levelSelector.SetActive (false);
			break;
		case 2:
			mainMenu.SetActive (false);
			shopPanel.SetActive (true);
			break;
		case 3:
			mainMenu.SetActive (true);
			shopPanel.SetActive (false);
			break;
		case 4:
			shopPanel.SetActive (true);
			ShowAdsPanel.SetActive (false);
			break;
		}


	}

	public void PlayBgSound()
	{
		if (PlayerPrefs.GetInt ("audio") == 0) {
			if (menuBg == null)
				return;
			source.clip = menuBg;
			source.Play ();
			source.loop = true;
		} else
			return;
	}

	private void SetLevelNumber()
	{
		for (int i = 0; i < levelBtnIndex.Length; i++) {
			levelBtnIndex [i].text = (i + 1).ToString ();
			if (i <= PlayerPrefs.GetInt ("level")) {
				levelBtn [i].SetActive (false);
				levelBtnBtn [i].interactable = true;
			}
		}
	}

	public void Play(int index)
	{
		selectedLevel = index;
		Application.LoadLevel ("Game");
	}

	public void SetText()
	{
		coinText.text = PlayerPrefs.GetInt ("coin").ToString ();

		itemValueTxt.text = PlayerPrefs.GetInt ("superthunder").ToString ();
		itemValueTxt1.text = PlayerPrefs.GetInt ("explosion").ToString ();
		itemValueTxt2.text = PlayerPrefs.GetInt ("thunder").ToString ();
		itemValueTxt3.text = PlayerPrefs.GetInt ("boom").ToString ();
	}

	public void SetItemValue()
	{
		for (int i = 0; i < shopItemValue.Length; i++) {
			shopItemValueText [i].text = shopItemValue[i].ToString ();
		}
	}


	public GameObject ShowAdsPanel;
	public void PurchaseItem(int index)
	{
		if (PlayerPrefs.GetInt ("coin") > shopItemValue [index]) {
			PlayerPrefs.SetInt (itemName [index], PlayerPrefs.GetInt (itemName [index]) + 1);
			PlayerPrefs.SetInt ("coin", PlayerPrefs.GetInt ("coin") - shopItemValue [index]);
			SetText ();
		} else {
			ShowAdsPanel.SetActive (true);
		}
	}
	public void ViewRewardVideo()
	{
		AdsControl.Instance.ShowRewardVideo ();
		ShowAdsPanel.SetActive (false);
		SetText ();
	}

	public GameObject IAPPanel;
	public GameObject ItemPanel;
	public void IPAShop(int index)
	{
		if (index == 0) {
			IAPPanel.SetActive (true);
			ItemPanel.SetActive (false);
		} else if (index == 1) {
			IAPPanel.SetActive (false);
			ItemPanel.SetActive (true);
		}
	}

}
