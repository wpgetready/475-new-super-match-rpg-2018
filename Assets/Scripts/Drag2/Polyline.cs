﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Polyline : MonoBehaviour
{
    public Material mat;
    public int thickness = 4;
    public int depth = 1;
    public Color color = Color.white;
    public float scale = 0.001428571f;
    public Vector3[] path;
    List<LineRenderer> lines;
    int total;

    void Start()
    {
        InitLines();
        DrawLines();
    }

    public void InitLines()
    {
        total = path.Length - 1;
        if (lines == null) lines = new List<LineRenderer>();
        for (int i = 0; i < total; i++)
        {
            LineRenderer r;
            if (i < lines.Count)
            {
                r = lines[i];
            }
            else
            {
                GameObject go = new GameObject("Line");
                go.layer = gameObject.layer;
                go.transform.SetParent(transform);
                go.transform.localScale = Vector3.one;
                r = go.AddComponent<LineRenderer>();
                r.material = mat;
                r.useWorldSpace = true;
                r.SetWidth(thickness * scale, thickness * scale);
                lines.Add(r);
            }
            r.enabled = false;
            r.SetColors(color, color);
        }
    }

    public void DrawLines()
    {
        for (int i = 0; i < total; i++)
        {
            LineRenderer r = lines[i];
            Vector3 v1 = path[i] * scale, v2 = path[i + 1] * scale;
            r.SetPosition(0, v1);
            r.SetPosition(1, v2);
            r.enabled = true;
        }
    }

    public void SetVisible(bool on)
    {
        foreach (LineRenderer r in lines)
        {
            r.enabled = on;
        }
    }
}